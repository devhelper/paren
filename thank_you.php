﻿<?php 
	include("includes/connection.php");
	include("includes/functions.php"); 
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		
		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{
			//your site secret key
			$secret = '6Lfm_hwUAAAAAEHLgs67XV2outIfPNANJqXK4Gxi';
			//get verify response data
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
			$responseData = json_decode($verifyResponse);
			if($responseData->success)
			{
				//contact form submission code
				$Navn = !empty($_POST['Navn'])?$_POST['Navn']:'';
				$Telefon = !empty($_POST['Telefon'])?$_POST['Telefon']:'';
				$Email = !empty($_POST['Email'])?$_POST['Email']:'';
				$Firmanavn = !empty($_POST['Firmanavn'])?$_POST['Firmanavn']:'';
				$Besked = !empty($_POST['Besked'])?$_POST['Besked']:'';
				
				mysql_query("INSERT INTO tbl_contact_enquiries SET Navn='$Navn', Telefon='$Telefon', Email='$Email', Firmanavn='$Firmanavn', Besked='$Besked', Create_Date=NOW()");
				
				$toemail="kontakt@dinenergibesparelse.dk";
				$res=SendEnquiry($toemail,$Navn,$Telefon,$Email,$Firmanavn,$Besked);
				if($res)
				{
					 $succMsg = 'Your contact request have submitted successfully.';
				}
				else
				{
					  $errMsg = 'Error occured while sending email.';
				} 
			   
			}
			else
			{
				$errMsg = 'Robot verification failed, please try again.';
			}
		}
		else
		{
			$errMsg = 'Please click on the reCAPTCHA box.';
		}
	}
	else
	{
		$errMsg = '';
		$succMsg = '';
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dinenergibesparelse - Få energitilskud</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">
	

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
	
	<link href="css/custom-css.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93110434-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '606208236199360'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=606208236199360&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

</head>

<body>
	 <div class="container">
	 	<div class="header-logo">
			<a href="index.php"><img src="img/logo.png" width="260px"></a>
		</div>
	</div>
    <!-- Header -->
    <div class="intro-header color-header">
        <div class="container">
			
            <div class="row">
                <div class="col-lg-12">
                    <h1>Thank you for contacting Us</h1>
					<p class="intro-text">
						We will get back to you soon.
					</p>
					
            	</div>
			</div>

        </div>
        <!-- /.container -->
    </div>
    <!-- /.intro-header -->
	 
	 <div class="intro-header white-color-block">
        <div class="container">
			
            <div class="row">
                <div class="col-lg-12">
                    <h1>Havad skal jeg gøre for at få energitilskud til mine sager?</h1>
					<div class="blocks">
						<div class="col-lg-3" style="display:inline-block; padding-right:0">
							<div class="icon-base-orange"><i class="fa fa-2x center-icon fa-file-o"></i></div>
							<h3 align="left">1. Søg tilskud inden opstarten</h3>
							<div class="clearfix"></div>
							<div style="margin-top:20px; text-align:left">
								<p>
								Ansøg dit tilskud eller kontakt os for at få beregninger inden opstart. Tilskuddet skal være ansøgt inden opstart. 
								</p>
							</div>
						</div>
						
						<div class="col-lg-3" style="display:inline-block; padding-right:0">
							<div class="icon-base-green"><i class="fa fa-2x center-icon fa-cog"></i></div>
							<h3 align="left">2. Udfør Arbejde</h3>
							<div class="clearfix"></div>
							<div style="margin-top:20px; text-align:left">
								<p>
								Når tilskuddet er ansøgt kan arbejdet udføres. Når arbejdet er udført færdigmeld sagen til os.
								</p>
							</div>
						</div>
						
						<div class="col-lg-3" style="display:inline-block; padding-right:0">
							<div class="icon-base-lightgreen"><i class="fa fa-2x center-icon fa-book"></i></div>
							<h3 align="left">3. Dokumentation</h3>
							<div class="clearfix"></div>
							<div style="margin-top:20px; text-align:left">
								<p>
								Fremsend og dokumentation for den ansøgte besparelse. Billeddokumentation, tilbud, faktura og skriftlig accept fra bygherre. 
								</p>
							</div>
						</div>
						
						<div class="col-lg-3" style="display:inline-block; padding-right:0">
							<div class="icon-base-darkgreen"><i class="fa fa-2x center-icon fa-money"></i></div>
							<h3 align="left">4. Tilskud udbetales</h3>
							<div class="clearfix"></div>
							<div style="margin-top:20px; text-align:left">
								<p>
								Tilskuddet udbetales 3-5 uger efter endt arbejde og godkendelse.
								</p>
							</div>
						</div>
					</div>
            	</div>
			</div>

        </div>
        <!-- /.container -->
    </div>
	 
	 <div class="intro-header green-color-block">
        <div class="container">
			
            <div class="row">
                <div class="col-lg-12">
                    <h1>Med beregninger og rådgivning fra os, kan du ofte øge mange af dine tilskud med 20-50% i forhold til standardværdier. Vi er specialister i isolering. Kontakt os i dag og få hjælp til håndtering af dine energitilskud.</h1>
            	</div>
			</div>

        </div>
        <!-- /.container -->
    </div>
	
	 <div class="intro-header white-color-block">
        <div class="container">
			
            <div class="row">
                <div class="col-lg-12">
                    <h1>Fordele ved energitilskud hos din energibesparelse</h1>
					<div class="blocks">
					<div class="col-lg-4" style="display:inline-block;">
						<div class="icon-base-blue"><span class="center-icon">1</span></div>
						<h3 align="left">Hurtigt og nemt</h3>
						<div class="clearfix"></div>
						<div style="margin-top:20px; text-align:left">
							<p>
							Kom hurtigt og nemt i gang med at tjene flere tusinde kroner ekstra på dine sager og bliv samtidigt mere konkurrencedygtig over for jeres konkurrenter. 
							</p>
						</div>
					</div>
					<div class="col-lg-4" style="display:inline-block; ">
						<div class="icon-base-blue"><span class="center-icon">2</span></div>
						<h3 align="left">Ingen adminstration</h3>
						<div class="clearfix"></div>
						<div style="margin-top:20px; text-align:left">
							<p>
							Slip for papirarbejde, bureaukrati og tilskudsberegning m.m.. Vi klarer det hele for dig, og regner også specifikke tilskud til dig og dine kunder, samt sikrer dig siden de korrekte udbetalinger. Alt sker elektronisk.
							</p>
						</div>
					</div>
					<div class="col-lg-4" style="display:inline-block;">
						<div class="icon-base-blue"><span class="center-icon">3</span></div>
						<h3 align="left">Nødvendig dokumentation</h3>
						<div class="clearfix"></div>
						<div style="margin-top:20px; text-align:left">
							<p>
							Boligarealer på over 200m2 kan vi beregne u-værdi forskellene for jer samt hjælpe med den nødvendige dokumentation.
							</p>
						</div>
					</div>
					
					
					</div>
            	</div>
			</div>

        </div>
        <!-- /.container -->
    </div>
	 
	 <div class="container">
            <div class="row">
				<div class="footer">
					<div class="footer-text footer-text-pad">
						Copyright 2016 by dinenergibesparelse.dk
					</div>
				</div>
			</div>
	</div>	
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 957228759;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/957228759/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Google Code for Din- Conversion Tracking Code Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 957228759;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "pc9-CIie7W4Q1824yAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/957228759/?label=pc9-CIie7W4Q1824yAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>

</html>
